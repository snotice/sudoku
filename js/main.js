require(['/js/sudoku.js'], function(sudoku){});
/**
 * represent a model object
 * @constructor
 */
function MNumber(){
    var self = this;
    /** propertyes **/
    self.value = m.prop("0");
    self.locked = m.prop(false);
    self.order = m.prop(0);
    /** @return {String} **/
    self.description = function(){
        return (self.value() === "0" ? "" : String(self.value()));
    };
}


/**
 * controller class
 * @constructor
 */
function MController(){
    var self = this;
    self.completed = m.prop(false);
    self.reset = _reset;
    self.input = _input;
    self.description = _description;
    self.update = _update;
    self.solve = _solve;



    /** @type {MNumber[]} */
    self.data = [];

    for(var i=0; i< 81; i++){
        var t = new MNumber();
        t.order(i);
        self.data.push(t);
    }

    self.prepare = function(){
        self.data.forEach(function(e){
            if (e.value() !== "0"){
                e.locked(true);
            }
        });
    };


    /** @returns {String} **/
    function _description(){
        return self.data.reduce(function(a, b){ return a + b.value();}, "");
    }

    
    function _update(numbers){
        for(var i=0; i<numbers.length; i++) {
            self.data[i].value(numbers[i]);
        }
    }

    
    function _input(){
        self.reset();
        var e = prompt("numbers");
        self.update(e);
        self.prepare();
    }

    function _solve(){
        self.prepare();
        var g = new Matrix(self.description());
        var r = g.solve();
        self.update(r);
        self.completed(true);
    }

    /**
     * Reset the grid
     * @return {void}
     */
    function _reset(){
        self.data.forEach(function(e){ e.value('0'); e.locked(false); });
        self.completed(false);
    }
}

/**
 * view class
 * @constructor
 * @param {MController} ctrl - controller object
 */
function MView(ctrl){
    return m('div', [
        m('button', {onclick:ctrl.solve.bind(ctrl), disabled:ctrl.completed()},"solve" ),
        m('button', {onclick:ctrl.reset.bind(ctrl)},"reset" ),
        m('button', {onclick:ctrl.input.bind(ctrl), disabled:ctrl.completed()},"input" ),
        m('div', {id: 'grid'}, [
            ctrl.data.map(function(e, i){
                var theClass = "number ";
                return m('div', {
                    id: 'div_' + i,
                    class: theClass,
                }, [m('input', {value: e.description(), 
                      'disabled': e.locked(),
                      'onchange': m.withAttr('value', e.value)
                })]);
            })
        ])
    ]);
}

function init(){
    var app = {};
    app.controller = MController;
    app.view = MView;
    if(window.minit === undefined){
        window.minit = true;
        m.module(document.getElementById('wrap'), app);
    }
}

document.onreadystatechange = init;
