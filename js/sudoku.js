/**
 * Represent a vertex
 * @constructor
 * @param {anyvalue} key : value of vertex
 */
function Vertex(value, order, matrix) {
    var self = this;
    /* declaration of public properties */
    self.color = 'white';
    self.availableNumbers = [];
    self.order = 0;
    self.value = 0;

    init(value, order);
    /** end of initiation **/

    /* internal methods */
    /**
     * initializer
     * @param {Number} value - value of vertex
     * @param {Number} order - order of verrtex in the matrix
     */
    function init(value, order) {
        self.order = order;
        self.value = value;
        if (value !== 0) {
            self.color = 'black';
        }
    }
}


/**
 * Sudoku matrix
 * @constructor
 * @param {String} - numbers
 */
function Matrix(numbers) {
    /** @constant */
    var WIDTH=9;
    var self = this;
    init(numbers);

    /**
     * initializer
     * @param {String} - 81characters length string composed with 1~9 numbers.
     */
    function init(numbers) {
        self.data = [];
        var l = numbers.length;
        for (var i = 0; i < WIDTH*WIDTH; i++) {
            var newValue = (i >= l ? 0 : parseInt(numbers[i]));
            var newVert = new Vertex(newValue, i);
            self.data.push(newVert);
        }
    }


    /** Public Methods **/

    self.draw = _draw;
    self.solve = runSolve2;


    /**
     * get numbers array at column
     * @param {Num} column
     * @return {Array}
     */
    function _getNumbersAtColumn(column) {
        var result = [];
        for (var i = 0; i < WIDTH; i++) {
            result.push(self.data[WIDTH * i + (column % WIDTH)].value);
        }
        return result;
    }



    /**
     * get numbers exists at given row
     * @param {Num}
     * @return {Array} - Array of numbers
     */
    function _getNumbersAtRow(row) {
        numbers = self.data.filter(function(e) {
            return row == Math.floor(e.order / WIDTH);
        }).map(function(e) {
            return e.value;
        });
        return numbers;
    }

    /**
     * get numbers exist at given column
     * @param {Num}
     * @return {Array}
     */
    function _getNumbersAtArea(order) {
        function areaFromOrder(o) {
            var row = Math.floor(o / WIDTH);
            var column = o % WIDTH;
            var area = Math.floor(row / 3) + Math.floor(column / 3) * 3;
            return area;
        }
        var area = areaFromOrder(order);

        numbers = self.data.filter(function(e) {
            return areaFromOrder(e.order) === area;
        }).map(function(e) {
            return e.value;
        });
        return numbers;
    }

    /**
     * find all available numbers at given order
     * @param {Num}
     * @return {Array}
     */
    function _availableNumbersAt(order) {
        var row = Math.floor(order / WIDTH);
        var column = order % WIDTH;
        var cands = [1, 2, 3, 4, 5, 6, 7, 8, 9];

        var availables = cands.filter(function(e) {
            /* filter by row */
            return _getNumbersAtRow(row).indexOf(e) == -1;
        }).filter(function(e) {
            /* filter by column */
            return _getNumbersAtColumn(column).indexOf(e) == -1;
        }).filter(function(e) {
            return _getNumbersAtArea(order).indexOf(e) == -1;
        });
        return availables;
    }



    /**
     * Draw 9 * 9 grid
     */
    function _draw() {
        numbers = self.data.map(function(e) {
            return e.value;
        });
        for (var i = 0; i < WIDTH; i++) {
            console.log(numbers.slice(i * WIDTH, (i+1)*WIDTH));
        }
        console.log("");
    }

    /**
     * Find available numbers for all remain white vertices
     */
    function _findAvailableNumbers() {
        var i = 0;
        while (i < WIDTH*WIDTH) {
            var v = self.data[i];
            if (v.color == 'white') {
                var availableNums = _availableNumbersAt(v.order);
                if (availableNums.length === 0) {
                    return false;
                }
                v.availableNumbers = availableNums;
            }
            i++;
        }
        return true;
    }

    /**
     * Find optimal next vertex.
     * it will be the one that has minimum number of available numbers.
     * if there is any dead end(has no available number), this returns
     * undefined.
     * @return {Vertex} || {undefined}
     */
    function _findOptimalNext() {
        var update = _findAvailableNumbers();
        if (update) {
            var vs = self.data.slice().sort().filter(function(e) {
                return e.color == 'white';
            })[0];
            return vs;
        }
        return undefined;
    }


    /**
     * run a Depth First Search and solve given sudoku
     */
    function runSolve2(){
        // optimize
        while (true) {
            _findAvailableNumbers();
            var toBeBlack = self.data.filter(function(e){ return e.availableNumbers.length == 1;});
            if (toBeBlack.length === 0){ break; }
            toBeBlack.forEach(function(e){ e.value = e.availableNumbers[0]; e.availableNumbers=[]; e.color='black';});
        }
        var n = _findOptimalNext();
        var r = visit_vertices(n);
        if (r) {
            self.draw();
            return self.data.reduce(function(e, d){ return e + d.value;}, "");
        }
        console.log('fail');
    }

    /**
     * find solution for sudoku puzzle
     * @param {Vertex} currentVert - current vertex object
     * @return {Bool} - true: ok. false: dead end
     */
    function visit_vertices(currentVert){
        if (self.data.filter(function(e){ return e.color == 'white'; }).length === 0) {
            return true;
        }
        while (currentVert.availableNumbers.length > 0) {
            currentVert.value = currentVert.availableNumbers.pop();
            currentVert.color = 'grey';
            if (self.data.filter(function(e){ return e.color == 'white'; }).length === 0) {
                return true;
            }
            var next = _findOptimalNext();
            if (next !== undefined) {
                var result = visit_vertices(next);
                if (result) {
                    return true;
                }
            }
        }
        currentVert.color = 'white';
        currentVert.value = 0;
        return false;
    }
}

function test() {
    sampe = "200080300060070084030500209000105408000000000402706000301007040720040060004010003";
    var m = new Matrix(sampe);
    console.log(m.solve());
}

define(function(){
    var exports = {
        version: "1.0"
    };

    exports.Matrix = Matrix;
    exports.test = test;
    return exports;
});
