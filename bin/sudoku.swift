#!/usr/bin/swift

/// Swift 1.2

class Node {
    let order: Int
    var value: Int
    var availables = Array<Int>()

    init(order:Int, value:Int) {
        self.order = order
        self.value = value
    }
}

class Grid {
    var data:Array<Node>

    init(data:String){
        let numbers = data.utf8.map{ Int($0) - 48 }
        self.data = numbers.enumerate().map{ Node(order:$0.0, value:$0.1) }
    }

    func positionByOrder(order:Int) -> (row:Int, col:Int, area:Int) {
        let row = order / 9
        let col = order % 9
        let area = 3 * (row / 3) + (col / 3)
        return (row, col, area)
    }

    func numbersAtRow(row:Int) -> Set<Int> {
        let s = Set(row*9..<(row+1)*9)
        return Set(s.map{ self.data[$0].value})
    }

    func numbersAtColumn(col:Int) -> Set<Int> {
        return Set(col.stride(to:81, by:9).map{ self.data[$0].value })
    }

    func numbersAtArea(area:Int) -> Set<Int> {
        let areas = self.data.filter{
            positionByOrder($0.order).area == area
        }.map{ $0.value}
        return Set(areas)
    }

    func availablesAt(order:Int) -> [Int] {
        let (row, col, area) = self.positionByOrder(order)
        let s = Set(1...9)
                .subtract(self.numbersAtRow(row)
                .union(self.numbersAtColumn(col)
                .union(self.numbersAtArea(area))))
        return Array(s)
    }

    func check() -> Bool {
        return self.data.filter{ $0.value == 0 }.count > 0
    }

    func draw() {
        for i in 0..<9 {
            let line = self.data[i*9..<(i+1)*9]
                .map{ String($0.value) }
                .joinWithSeparator(",")
            print(line)
        }
        print("\n")
    }
    
    func solve() {
        var stack = [Node]()
        var direction = 1
        var g:Node!
        while check() {
            if direction == 1 {
                g = self.data.filter{ $0.value == 0 }[0]
                g.availables = availablesAt(g.order)
                guard g.availables.count > 0 else {
                    g.value = 0
                    direction = 0
                    continue
                }
                g.value = g.availables.removeLast()
                stack.append(g)
            } else {
                guard stack.count > 0 else {
                    print("Malformed Sudoku")
                    break
                }
                g = stack.removeLast()
                guard g.availables.count > 0 else {
                    g.value = 0
                    continue
                }
                g.value = g.availables.removeLast()
                stack.append(g)
                direction = 1
            }
        }
        draw()
    }
}


func timeit(@noescape f:()->()) {
    let s = NSDate()
    f()
    let e = NSDate().timeIntervalSinceDate(s)
    print("time: \(e * 1000) ms")
}

timeit{
    let d:String = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
    let g = Grid(data:d)
    g.solve()
}
