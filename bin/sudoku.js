/**
 * Represent a vertex
 * @constructor
 * @param {anyvalue} key : value of vertex
 */
function Vertex(value, order, matrix) {
    var self = this;
    /* declaration of public properties */
    self.color = 'white';
    self.availableNumbers = [];
    self.order = 0;
    self.value = 0;


    init(value, order);
    /** end of initiation **/

    /* internal methods */
    /**
     * initializer
     * @param {Number} value - value of vertex
     * @param {Number} order - order of verrtex in the matrix
     */
    function init(value, order) {
        self.order = order;
        self.value = value;
        if (value !== 0) {
            self.color = 'black';
        }
    }


    /**
     * connect to other vertex
     * @param {Vertex} vert - vertex that is going to be a suceessor
     */
    function _addConnection(vert) {
        self.successors[vert.order] = vert.value;
    }
}



/**
 * Sudoku matrix
 * @constructor
 * @param {String} - numbers
 */
function Matrix(numbers) {
    var self = this;
    init(numbers);

    /**
     * initializer
     * @param {String} - numbers string.
     */
    function init(numbers) {
        self.data = [];
        var l = numbers.length;
        for (var i = 0; i < 81; i++) {
            var newValue = (i >= l ? 0 : parseInt(numbers[i]));
            var newVert = new Vertex(newValue, i);
            self.data.push(newVert);
        }
    }


    /** Public Methods **/

    self.getNumbersAtColumn = _getNumbersAtColumn;
    self.getNumbersAtRow = _getNumbersAtRow;
    self.getNumbersAtArea = _getNumbersAtArea;
    self.availableNumbersAt = availableNumbersAt;
    self.draw = _draw;
    self.run = runBFS;


    /**
     * get numbers array at column
     * @param {Num} column
     * @return {Array}
     */
    function _getNumbersAtColumn(column) {
        var result = [];
        for (var i = 0; i < 9; i++) {
            result.push(self.data[9 * i + (column % 9)].value);
        }
        return result;
    }



    /**
     * get numbers exists at given row
     * @param {Num}
     * @return {Array} - Array of numbers
     */
    function _getNumbersAtRow(row) {
        numbers = self.data.filter(function(e) {
            return row == Math.floor(e.order / 9);
        }).map(function(e) {
            return e.value;
        });
        return numbers;
    }

    /**
     * get numbers exist at given column
     * @param {Num}
     * @return {Array}
     */
    function _getNumbersAtArea(order) {
        function areaFromOrder(o) {
            var row = Math.floor(o / 9);
            var column = o % 9;
            var area = Math.floor(row / 3) + Math.floor(column / 3) * 3;
            return area;
        }
        var area = areaFromOrder(order);

        numbers = self.data.filter(function(e) {
            return areaFromOrder(e.order) === area;
        }).map(function(e) {
            return e.value;
        });
        return numbers;
    }

    /**
     * find all available numbers at given order
     * @param {Num}
     * @return {Array}
     */
    function availableNumbersAt(order) {
        var row = Math.floor(order / 9);
        var column = order % 9;
        var cands = [1, 2, 3, 4, 5, 6, 7, 8, 9];

        var availables = cands.filter(function(e) {
            /* filter by row */
            return self.getNumbersAtRow(row).indexOf(e) == -1;
        }).filter(function(e) {
            /* filter by column */
            return self.getNumbersAtColumn(column).indexOf(e) == -1;
        }).filter(function(e) {
            return self.getNumbersAtArea(order).indexOf(e) == -1;
        });
        return availables;
    }



    /**
     * Draw 9 * 9 grid
     */
    function _draw() {
        numbers = self.data.map(function(e) {
            return e.value;
        });
        for (var i = 0; i < 9; i++) {
            console.log(numbers.slice(i * 9, i * 9 + 9));
        }
    }

    /**
     * Find available numbers for all remain white vertices
     */
    function findAvailableNumbers() {
        var i = 0;
        while (i < 81) {
            var v = self.data[i];
            if (v.color == 'white') {
                var availableNums = self.availableNumbersAt(v.order);
                if (availableNums.length === 0) {
                    return false;
                }
                v.availableNumbers = availableNums;
            }
            i++;
        }
        return true;
    }

    /**
     * Find optimal next vertex.
     * it will be the one that has minimum number of available numbers.
     * if there is any dead end(has no available number), this returns
     * undefined.
     * @return {Vertex} || {undefined}
     */
    function findOptimalNext() {
        var update = findAvailableNumbers();
        if (update) {
            var vs = self.data.slice().sort().filter(function(e) {
                return e.color == 'white';
            })[0];
            return vs;
        }
        return undefined;
    }


    /**
     * run a Depth First Search and solve given sudoku
     */
    function runBFS() {
            //var qeueu = new Queue();

            var direction = 1; // 1: forward, 0: backward.
            var stack = [];

            /** @constant {Num} */
            var limit = 80 - self.data.filter(function(e) {
                return e.color == 'black';
            }).length;
            /** @type {Vertex} */
            var next;
            while (true) {
                if (stack.length == limit) {
                    self.draw();
                    break;
                } else if (next === undefined) {
                    direction = 1;
                }
                if (direction == 1) {
                    /* going forward */
                    next = findOptimalNext();
                    if (next !== undefined) {
                        next.color = 'grey';
                        next.value = next.availableNumbers[0];
                        next.availableNumbers = next.availableNumbers.slice(1, next.availableNumbers.length);
                        stack.push(next);
                    } else {
                        // meet dead end;
                        direction = 0;
                        next = stack.pop();
                    } // end of block - if direction is 1
                } else {
                    /* going backwards */
                    while (next.color == 'black') {
                        next = stack.pop();
                    }
                    if (next.availableNumbers.length > 0) {
                        // if there is one or more available numbers,
                        direction = 1;
                        next.value = next.availableNumbers[0];
                        next.availableNumbers = next.availableNumbers.slice(1, next.availableNumbers.length);
                        stack.push(next);
                    } else {
                        /* go backward one step more */
                        next.value = 0;
                        next.color = 'white';
                        next = stack.pop();
                    }
                } // end of block - goign backward
            } // End of Loop
        } // End of Function
}

//var sample = "003020600900305001001806400008102900700000008006708200002609500800203009005010300";
var sample = "200080300060070084030500209000105408000000000402706000301007040720040060004010003";
var m = new Matrix(sample);
m.run();
