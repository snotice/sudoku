class Node:
    def __init__(self, order, value):
        self.order = order
        self.value = value
        self.availables = []

class Grid:
    def __init__(self, data: str):
        self.data = [Node(i, int(v)) for i, v in enumerate(data[:81])]
        self.size = 9

    def positionByOrder(self, order):
        row = order // self.size
        col = order % self.size
        area = 3 * (row // 3) + col // 3
        return row, col, area

    def numbersAtRow(self, row):
        return set((self.data[x].value\
                    for x in range(row * self.size, 
                                   (row + 1) * self.size)))

    def numbersAtCol(self, col):
        return set((self.data[x].value\
                    for x in range(col, 
                                   self.size**2, self.size)))

    def numbersAtArea(self, area):
        return set((x.value for x in self.data\
                    if self.positionByOrder(x.order)[2] == area))

    def availablesAt(self, order):
        row, col, area = self.positionByOrder(order)
        sieve = self.numbersAtRow(row).union(
                    self.numbersAtCol(col).union(
                        self.numbersAtArea(area)))
        return [x for x in range(1, 10) if x not in sieve]

    def check(self):
        return len({x for x in self.data if x.value is 0}) > 0

    def draw(self):
        for i in range(9):
            print(", ".join(
                [str(x.value) for x in self.data[i*self.size:(i+1)*self.size]]))
        print('\n')

    def solve(self):
        direction = 1
        stack = []
        while self.check():
            if direction:
                g = [x for x in self.data if x.value is 0][0]
                g.availables = self.availablesAt(g.order)
                if not g.availables:
                    direction = 0
                    continue
                g.value = g.availables.pop()
                stack.append(g)
                continue
            else:
                if stack == []:
                    print("Malformed Sudoke!!!")
                    break
                g = stack.pop()
                if not g.availables:
                    g.value = 0
                    continue
                g.value = g.availables.pop()
                stack.append(g)
                direction = 1
        self.draw()


d = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
g = Grid(d)
g.solve()
