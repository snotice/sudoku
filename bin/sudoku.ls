class Node
    (@order, @value) ->
        @availables = []

class Grid
    (data) ->
        @size = 9
        @data = [0 til @size**2].map ->
            new Node it, parseInt data[it]

    position-by-order: (order) ~>
        row = Math.floor <| order / @size
        col = order % @size
        area = 3 * (Math.floor <| row / 3) + (Math.floor <| col / 3)
        return row: row, col: col, area: area

    numbers-at-row: (row) ~>
        @data[row*@size til (row+1)*@size].map (.value)

    numbers-at-col: (col) ~>
        @data[col to @size**2 by @size].map (.value)

    numbers-at-area: (area) ~>
        orders = [til @size**2].filter ~>
            @position-by-order it
            .area == area
        return orders.map ~> @data[it].value

    availables-at: (order) ~>
        {row:row, col:col, area:area} = @position-by-order order
        [1 to 9].filter ~>
            it not in @numbers-at-row row
        .filter ~>
            it not in @numbers-at-col col
        .filter ~>
            it not in @numbers-at-area area

    check: ~>
        @data.filter (.value == 0)
        .length > 0

    draw: !~>
        for i in [til @size]
            @data[i*@size til (i+1)*@size]
                .map (.value)
                .join ", "
            |> console.log
        console.log ""

    solve: !~>
        stack = []
        direction = 1
        g = void
        while @check!
            if direction is 1 then
                g = @data.filter (.value == 0) .[0]
                g.availables = @availables-at g.order
                if g.availables.length is 0 then
                    g.value = 0
                    direction = 0
                    continue
                g.value = g.availables.pop!
                stack.push g
            else
                if stack.length is 0 then
                    console.error "Mallformed Sudoku."
                    break
                g = stack.pop!
                if g.availables.length is 0 then 
                    g.value = 0
                    continue
                g.value = g.availables.pop!
                stack.push g
                direction = 1
                continue
        @draw!

d = \003020600900305001001806400008102900700000008006708200002609500800203009005010300
g = new Grid d
g.solve!
